Installation Guide

- Clone Git repository
- Docker und Docker Compse installieren
- In das Verzeichnis git Repository Tarent wechseln
- Anschließend den Befehl "docker-compose up"

URL zum Abruf der API Beschreibung

http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs


Um das Frontend aufzurufen die URL http://localhost:3000/ nutzen




Sicherheitsideen

- HTTPs zur Kommunikation nutzen
- Falls Passwörter versendet werden immer gehasht
- Niemals apiKeys, usernamen, passwörter etc über die URL mitsenden
- Eventuell OAuth nutzen
- Eventuell Timestamp im Request mitsenden und auf Serveseite überprüfen#
- Eingabeparameter überprüfen
- Wenn Datenbank dahinter dann immer Befehle prüfen (SQL-Injection)
- Nutzung von Tokens (API-Schlüssel)
- Whitelisting von Requests. Nur die nötigen anbieten
- Eingabetypen validieren (ist es ein application/xml oder application/json?)