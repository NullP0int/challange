package tarent.challenge.schulung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchulungApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchulungApplication.class, args);
    }

}
