package tarent.challenge.schulung.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Termine einer Schulung")
public class Termin {

    private String start;
    private String end;

    @Schema(description = "Startdatum der Schulung", example = "01.06.2020")
    public String getStart() {
        return this.start;
    }

    @Schema(description = "Enddatum der Schulung", example = "07.06.2020")
    public String getEnd() {
        return this.end;
    }

}
