package tarent.challenge.schulung.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Beschreibung einer Schulung")
public class Schulung {

    private int id;
    private String name;
    private String beschreibung;
    private String dozName;
    private double preis;
    private String start;
    private String end;

    // Default constructor
    public Schulung() {

    }

    // Constructor
    public Schulung(int id, String name, String beschreibung, String dozName, double preis, String start, String end) {
        this.id = id;
        this.name = name;
        this.beschreibung = beschreibung;
        this.dozName = dozName;
        this.preis = preis;
        this.start = start;
        this.end = end;
    }

    @Schema(description = "Id der Schulung", example = "1")
    public int getId() {
        return this.id;
    }

    @Schema(description = "Name der Schulung", example = "Java")
    public String getName() {
        return this.name;
    }

    @Schema(description = "Beschreibung der Schulung", example = "Programmieren in Java")
    public String getBeschreibung() {
        return this.beschreibung;
    }

    @Schema(description = "Dozentenname", example = "Max Mustermann")
    public String getDozName() {
        return this.dozName;
    }

    @Schema(description = "Preis der Schulung in Euro", example = "9999.99")
    public double getPreis() {
        return this.preis;
    }

    @Schema(description = "Startdatum der Schulung", example = "01.06.2020")
    public String getStart() {
        return this.start;
    }

    @Schema(description = "Enddatum der Schulung", example = "07.06.2020")
    public String getEnd() {
        return this.end;
    }
}
