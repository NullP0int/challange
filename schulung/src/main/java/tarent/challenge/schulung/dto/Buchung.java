package tarent.challenge.schulung.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Buchung einer Schulung")
public class Buchung {

    private int schulungsId;
    private String vorname;
    private String nachname;
    private String strasse;
    private String hausnummer;
    private String plz;
    private String IBAN;

    @Schema(description = "Id der Schulung", example = "1")
    public int getSchulungsId() {
        return this.schulungsId;
    }

    @Schema(description = "Vorname", example = "Max")
    public String getVorname() {
        return this.vorname;
    }

    @Schema(description = "Nachname", example = "Mustermann")
    public String getNachname() {
        return this.nachname;
    }

    @Schema(description = "Strasse", example = "Musterstraße")
    public String getStrasse() {
        return this.strasse;
    }

    @Schema(description = "Hausnummer", example = "11b")
    public String getHausnummer() {
        return this.hausnummer;
    }

    @Schema(description = "Postleitzahl", example = "123456")
    public String getPlz() {
        return this.plz;
    }

    @Schema(description = "IBAN", example = "DE....")
    public String getIBAN() {
        return this.IBAN;
    }

}
