package tarent.challenge.schulung.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import tarent.challenge.schulung.dto.Buchung;
import tarent.challenge.schulung.dto.Schulung;
import tarent.challenge.schulung.dto.Termin;

import java.util.ArrayList;
import java.util.List;

@RestController
@EnableWebMvc
@RequestMapping("/api")
@Tag(name = "Schulungen", description = "SchulungsAPI")
public class SchulungController {

    // Liste um alle Schulungen zu speichern
    private List<Schulung> Schulungen = new ArrayList<>();

    @Operation(summary = "Ausgabe aller angebotenen Schulungen als JSON Array", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rückgabe aller Schulungen", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Schulung.class)))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @GetMapping(value = "schulungen", produces = {"application/json"})
    public List<Schulung> getAll() {
        return Schulungen;
    }


    @Operation(summary = "Ausgabe aller angebotenen Schulungen als JSON Array in einem bestimmten Zeitraum", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rückgabe aller Schulungen in einem bestimmten Zeitraum", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Schulung.class)))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @GetMapping(value = "schulungen/{dateVon}/{dateBis}", produces = {"application/json"})
    public List<Schulung> getSchulungByDate(
            @Parameter(description = "Startdatum der Schulung, Format 01.01.2020", required = true) @PathVariable String dateVon,
            @Parameter(description = "Enddatum der Schulung, Format 01.01.2020", required = true) @PathVariable String dateBis
    ) {
        return Schulungen;
    }


    @Operation(summary = "Ausgabe aller Termine zu einer bestimmten Schulung", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rückgabe Termine zu einer Schulung", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(type = "object", implementation = Termin.class)))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @GetMapping(value = "schulungen/{nameDerSchulung}", produces = {"application/json"})
    public Termin getSchulungsTermine(
            @Parameter(description = "Name der Schulung", required = true) @PathVariable String name
    ) {
        return new Termin();
    }


    @Operation(summary = "Anlegen einer neuen Schulung", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Schulung erfolgreich angelegt", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "400", description = "Angabe des Bodys fehlerhaft", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @PostMapping(value = "schulungen", produces = {"application/json"})
    public void createSchulung(
            @RequestBody(required = true) Schulung schulung
    ) {
        this.Schulungen.add(schulung);
    }

    @Operation(summary = "Update einer Schulung", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schulung erfolgreich upgedated", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "400", description = "Angabe des Bodys fehlerhaft", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "404", description = "Schulung nicht gefunden", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @PatchMapping(value = "schulungen", produces = {"application/json"})
    public String patchSchulung(
            @RequestBody(required = true) Schulung schulung
    ) {
        return("OK");
    }


    @Operation(summary = "Update eines Termins einer Schulung", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Termin erfolgreich upgedated", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "400", description = "Angabe des Bodys fehlerhaft", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "404", description = "Schulung nicht gefunden", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @PatchMapping(value = "schulungen/termin", produces = {"application/json"})
    public String patchSchulungsTermin(
            @RequestBody(required = true) int SchulungsId, Termin termin
    ) {
        return("OK");
    }

    @Operation(summary = "Buchen einer Schulung", description = "", tags = { "contact" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schulung erfolgreich gebucht", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "400", description = "Angabe des Bodys fehlerhaft", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "404", description = "Schulung nicht gefunden", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @PostMapping(value = "schulungen/buchen", produces = {"application/json"})
    public String schulungBuchen(
             @RequestBody(required = true) Buchung buchung
    ) {
        return("OK");
    }


}
