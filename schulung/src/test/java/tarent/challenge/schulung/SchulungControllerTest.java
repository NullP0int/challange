package tarent.challenge.schulung;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import tarent.challenge.schulung.controller.SchulungController;
import tarent.challenge.schulung.dto.Schulung;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SchulungController.class })
@WebAppConfiguration
public class SchulungControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    /* @Test
    public void testGetAllSchulungen() throws Exception {

        // Test für die Get Route /api/schulungen.
        mockMvc.perform(MockMvcRequestBuilders.get("/api/schulungen"));
                //.andExpect(status().isOk());
    } */

    @Test
    public void postSchulung() throws Exception {

        // Setzt eine Post an die Route /api/schulungen ab

        // Erstellen eines SchulungsObjects
        Schulung schulung = new Schulung(1, "Java", "Programmieren in Java", "Max Mustermann", 9999.99, "01.06.2020", "07.06.2020");

        // In Json umwandeln
        String json = new Gson().toJson(schulung);

        // Post it
        mockMvc.perform(MockMvcRequestBuilders.post("/api/schulungen")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }
}
