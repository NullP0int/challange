
URL zum Abruf der API Beschreibung

http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs


Sicherheit:

- HTTPs zur Kommunikation nutzen
- Falls Passwörter versendet werden immer gehasht
- Niemals apiKeys, usernamen, passwörter etc über die URL mitsenden
- Eventuell OAuth nutzen
- Eventuell Timestamp im Request mitsenden und auf Serveseite überprüfen#
- Eingabeparameter überprüfen
- Wenn Datenbank dahinter dann immer Befehle prüfen (SQL-Injection)
- Nutzung von Tokens (API-Schlüssel)
- Whitelisting von Requests. Nur die nötigen anbieten
- Eingabetypen validieren (ist es ein application/xml oder application/json?)