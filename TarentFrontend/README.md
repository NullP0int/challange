Frontend Implementierung

Um das Programm zu starten kann entweder NPM genutzt werden oder Docker

Installation Guide mit NPM

- Clonen des Gitlab Repos mit dem Befehl "git clone https://gitlab.com/NullP0int/schulungenfrontend.git"
- Download NPM
- In das Verzeichnis "schulungenFrontend" wechseln
- Installieren der Paketabhängigkeiten mit dem Befehl "npm install"
- Starten der Applikation mit "npm start"
- Das Frontend ist nun über die URL "http://localhost:3000" erreichbar

Installation Guide mit Docker

- Clonen des Gitlab Repos mit dem Befehl "git clone https://gitlab.com/NullP0int/schulungenfrontend.git"
- In das Verzeichnis "schulungenFrontend" wechseln
- Erstellen des Images mit dem Befehl "docker build -t frontend ."
- Starten des Containers mit dem Vorher erstellen Image mit dem Befehl "docker run --name frontend -p 3000:3000 frontend" 
- Das Frontend ist nun über die URL "http://localhost:3000" erreichbar


Wichtig: Das Backend muss vorher gestartet werden, da im Frontend momentan noch keine Überprüfung stattfindet ob das Backend erreichbar ist oder nicht.



https://stackoverflow.com/questions/42385977/accessing-a-docker-container-from-another-container