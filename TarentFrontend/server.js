"use strict";

const app = require("./app");

app.listen(3000, () => {
    console.log("Frontend started on port 3000");
});