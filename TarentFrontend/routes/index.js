var express = require('express');
var router = express.Router();

// Mapping von der index Datei des Ordners ./home auf die Route /
router.use('/', require('./home'));

module.exports = router;