let express = require('express');
let router = express.Router();
let backendConnector = require('../../functions/backendConnector');


// Get route
router.get('/', function (req, res) {

    let data = {};

    // Absenden des Backendrequests um alle angebotenen Schulungen einzusehen
    let schulungenProm = backendConnector.sendBackendPost('api/schulungen', data, 'GET');

        schulungenProm.then(function(result) {
            console.log(result);
            // Übergabe der Schulungen an die View home/index
            res.render("home/index", {schulungen: result});
        }).catch(function(error) {
            console.log(error.name);
            console.log(error);
            res.render("home/index");
        });
});


// Post route /createSchulung
router.post('/createSchulung', function (req, res) {

    // Überprüfung ob alle Parameter angegeben wurden
    if(!req.body.id || !req.body.name || !req.body.beschreibung || !req.body.dozent || !req.body.Preis || !req.body.start || !req.body.end) {
        console.log('Schulung konnte nicht erstellt werden missing arguments');
        res.redirect("/");
    } else {
        // Schulungsobject erzeugen
        let data = {
            id: req.body.id,
            name: req.body.name,
            dozent: req.body.dozent,
            preis: req.body.Preis,
            start: req.body.start,
            end: req.body.end
        }

        // Senden es Objects an das Backend
        let schulungPostProm = backendConnector.sendBackendPost('api/schulungen', data, 'POST');

        schulungPostProm.then(function(result) {
            console.log(result);
            res.redirect("/");
        });
    }
});

module.exports = router;