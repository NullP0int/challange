"use strict";

const express = require("express");

const app = express();

// body parser
let bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// end body parser

// Setzen der Plugins
app.set("views", "./views");
app.set("view engine", "pug");

// Aufsetzen der Routen
app.use("/", require('./routes'));

module.exports = app;