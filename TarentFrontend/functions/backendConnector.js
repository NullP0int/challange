'use strict';

// Dependencies
const request = require('request');


// Function um einen Backend Request abzusetzen
function sendBackendPost(route, data, method) {

    return new Promise((resolve, reject) => {
        request({
            url: 'http://backend:8080/' + route,
            body: data,
            method: method,
            json: true
        }, (err, res, body) => {
            if(err){
                reject(err);
            }
            resolve(body);
        });
    }) 
}

// Exports
module.exports.sendBackendPost = sendBackendPost;